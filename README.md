# Build

The container build is integrated in the GitLab CI, but if you want to build manually:
```
docker build -t gitlab-registry.cern.ch/cloud-infrastructure/fouine-hammer .
```

# Sample Run

First create an openstack token, to be passed as a secret:
```
rbritoda@ciadm03 ~$$ openstack token issue
+------------+--------------------------------------+
| Field      | Value                                |
+------------+--------------------------------------+
| expires    | 2017-03-02 10:53:14+00:00            |
| id         | 135471abe10a45c4850bd959018b3905     |
| project_id | 629ec518-1b4b-4565-a43c-2627429e19bd |
| user_id    | rbritoda                             |
+------------+--------------------------------------+
```

Edit fouine-hammer.yaml file to pass your username and project ID appropriately, and then:
```
$ kubectl delete secret ostoken
$ kubectl create secret generic ostoken --from-literal=ostoken=135471abe10a45c4850bd959018b3905
secret "ostoken" created

$ kubectl create -f fouine-hammer.yaml

$ kubectl get pod
NAME                             READY     STATUS    RESTARTS   AGE
fouine-hammer-3638474706-gfubf   1/1       Running   0          3s

$ kubectl logs -f fouine-hammer-3638474706-gfubf
+--------------------------------------+--------------+------+-------------+--------+-----------+-----------------------+------+-------------------+
| ID                                   | Name         | Size | Share Proto | Status | Is Public | Share Type Name       | Host | Availability Zone |
+--------------------------------------+--------------+------+-------------+--------+-----------+-----------------------+------+-------------------+
| 407bd521-f454-41a7-93a9-ca21da1234ad | cephshare001 | 1    | CEPHFS      | error  | False     | Geneva CephFS Testing |      | None              |
+--------------------------------------+--------------+------+-------------+--------+-----------+-----------------------+------+-------------------+
Sleeping for 10 seconds...
```

You can update the number of replicas of the deployment:
```
kubectl edit deployment/fouine-hammer
```
