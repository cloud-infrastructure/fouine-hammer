#!/bin/bash

export OS_USERNAME=${1:-rbritoda}
export OS_PROJECT_ID=${2:-629ec518-1b4b-4565-a43c-2627429e19bd}
export OS_AUTH_URL="https://keystone.cern.ch/main/v2.0"
export OS_IDENTITY_API_VERSION="3"
export OS_REGION_NAME="cern"
export OS_MANILA_BYPASS_URL="https://openstack.cern.ch:8786/v2/${OS_PROJECT_ID}"

SLEEP=${3:-1}
TEST=${4:-list}

case $TEST in
    list )
        while true; do
            manila list
        done ;;
    access )
        ID=`manila create --share-type "Geneva CephFS Testing" cephfs 1|grep "| id"|awk '{print $4}'`
        while true; do
            echo "access"
            sleep ${SLEEP}
            AID=`manila access-allow ${ID} cephx wiebalck|grep "| id"|awk '{print $4}'`
            manila access-deny ${ID} ${AID} 
            sleep ${SLEEP}
        done ;; 
    createdelete )
        while true; do
            ID=`manila create --share-type "Geneva CephFS Testing" cephfs 1|grep "| id"|awk '{print $4}'`
            echo "created share ${ID}, sleep for ${SLEEP} seconds ..."
            sleep ${SLEEP}
            while true; do
              echo "check existence of ${ID}"
              manila list|grep available|grep --silent ${ID}
              if [ $? -eq 0 ]; then break; fi
            done
            manila delete ${ID}
            echo "deleted share ${ID}, sleep for ${SLEEP} seconds ..."
            sleep ${SLEEP}
        done ;;
esac
