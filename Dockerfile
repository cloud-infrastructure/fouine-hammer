FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:latest

ADD http://linux.web.cern.ch/linux/centos7/CentOS-CERN.repo /etc/yum.repos.d/CentOS-CERN.repo
RUN /usr/bin/rpm --import http://linuxsoft.cern.ch/cern/centos/7.1/os/x86_64/RPM-GPG-KEY-cern

RUN yum install -y \
	ca-certificates \
	yum-plugin-priorities

# CERN CA
ADD cerngridca.crt /etc/pki/ca-trust/source/anchors/cerngridca.crt
ADD cernroot.crt /etc/pki/ca-trust/source/anchors/cernroot.crt
RUN update-ca-trust

# openstack clients
RUN echo $'\n\
[cci7-openstack-clients-stable] \n\
name=CERN rebuilds for OpenStack clients - QA \n\
baseurl=http://linuxsoft.cern.ch/internal/repos/openstackclients7-newton-stable/x86_64/os/ \n\
enabled=1 \n\
gpgcheck=0 \n\
priority=1 \n'\
>> /etc/yum.repos.d/openstackclients7-newton-stable.repo

RUN yum install -y \
	centos-release-openstack-newton
RUN sed -i 's/enabled=1/enabled=1\npriority=1/' /etc/yum.repos.d/CentOS-OpenStack-newton.repo

RUN yum install -y \
	python-cryptography \
	python-decorator \
	python-keystoneclient-x509 \
	python-openstackclient \
	python2-manilaclient

ENV SHELL=bash

ADD run.sh /run.sh

CMD ["1"]

ENTRYPOINT ["/run.sh"]
